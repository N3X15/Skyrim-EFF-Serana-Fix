Scriptname EFFSeranaFix extends EFFPlugin Conditional


int Property PLUGIN_EVENT_CLEAR_ALL = -1 Autoreadonly
int Property PLUGIN_EVENT_WAIT = 0x04 Autoreadonly
int Property PLUGIN_EVENT_SANDBOX = 0x05 Autoreadonly
int Property PLUGIN_EVENT_FOLLOW = 0x03 Autoreadonly
int Property PLUGIN_EVENT_ADD_FOLLOWER = 0x00 Autoreadonly
int Property PLUGIN_EVENT_REMOVE_FOLLOWER = 0x01 Autoreadonly
int Property PLUGIN_EVENT_REMOVE_DEAD_FOLLOWER = 0x02 Autoreadonly

; Serana's follow script.
DLC1_NPCMentalModelScript Property MM Auto

; Debug constant.
bool DebugSpam = false

; Standard initialization for the plugin, DO NOT EDIT UNLESS YOU KNOW WHAT YOU'RE DOING
Event OnInit()
	if DebugSpam
		Debug.Notification("Starting SeranaFix.")
		if XFLMain
			Debug.Notification("XFLMain is present")
		EndIf
		if FollowerMenu
			Debug.Notification("FollowerMenu is present")
		EndIf
	EndIf
	If XFLMain && FollowerMenu
		if DebugSpam
			Debug.Notification("Registering SeranaFix with EFF")
		EndIf
		XFLMain.XFL_RegisterPlugin(Self) ; Register the plugin
		If MenuOption
			MenuRef = FollowerMenu.XFL_RegisterMenuOption(MenuOption) ; Add the option to the list
		EndIf
	EndIf
EndEvent


; Determines whether to show the plugin in the command menu
Bool Function showMenu(Form akForm) ; Re-implement
	return false
EndFunction

; Determines whether to show the plugin in the group command menu
Bool Function showGroupMenu() ; Re-implement
	return false
EndFunction

int Function GetIdentifier()
	return 0x2000
EndFunction

string Function GetPluginName()
	return "Serana Fix"
EndFunction

Function XFL_ForceClearAll()
	MM.Dismiss()
EndFunction

Event OnDisabled()
	MM.Dismiss()
EndEvent

; All of these plugin events will stop the follower from collecting
Event OnPluginEvent(int akType, ObjectReference akRef1 = None, ObjectReference akRef2 = None, int aiValue1 = 0, int aiValue2 = 0)
  if akRef1 == MM.RNPC.GetActorReference() ; Must be Serana for us to give a flying shit.
		if MM.QuestLineCompleted == true ; Questline must be complete.
	  	If akType == PLUGIN_EVENT_WAIT ; Told Serana to wait
				if DebugSpam
					Debug.Notification("Serana: Wait() called.")
				endIf
	  		MM.Wait()
	    ElseIf akType == PLUGIN_EVENT_FOLLOW ; Told to follower
				if DebugSpam
					Debug.Notification("Serana: EngageFollowBehavior() called.")
				endIf
	      MM.EngageFollowBehavior(true) ; Force her to follow.
	    ElseIf akType == PLUGIN_EVENT_SANDBOX
				; ???
	    ElseIf akType == PLUGIN_EVENT_ADD_FOLLOWER
				if DebugSpam
					Debug.Notification("Serana: EngageFollowBehavior() called.")
				endIf
	      MM.EngageFollowBehavior(true)
	    ElseIf akType == PLUGIN_EVENT_REMOVE_FOLLOWER || akType == PLUGIN_EVENT_REMOVE_DEAD_FOLLOWER
				if DebugSpam
					Debug.Notification("Serana: Dismiss() called.")
				endIf
	      MM.Dismiss()
	  	Elseif akType == -1 ; Clearall happens before the actual event
	  		XFL_ForceClearAll()
	  	Endif
		Endif
  Endif
EndEvent
