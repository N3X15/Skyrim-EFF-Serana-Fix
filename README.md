# OVERVIEW

EFF Serana Fix is a simple mod intended for users of EFF, UFO, AFT, or other follower overhauls who cannot get Serana to follow them if they already have followers.  

Included is an EFF plugin that "kicks" her Mental Model script, which is used by DLC quests to set her follow/wait states forcefully.  The Mental Model Kicker translates
EFF's commands to the appropriate function calls to Serana's mental model.  This is done to fix an issue where the script forces her to walk home, even if she's forced
to follow the player with EFF.  Adding a FOMOD installer to make this script optional, and making equivalents for UFO and AFT are the next goals of this project.

# Installation

We're on the [Skyrim Nexus](http://www.nexusmods.com/skyrim/mods/76585/?) with an NMM-accessible download.

## Requirements

* DawnGuard DLC
* Extensible Follower Framework (http://www.nexusmods.com/skyrim/mods/12933/?)

## Process

1. Download and install the ESP and scripts into your Skyrim Data/ directory.

It should then just work.

# Compiling

At the moment, this project uses my private papyrus compile environment (thus the papyrus.yml), which isn't ready for public release yet. I don't even have a name for it.

However, standard papyrus compile procedures should work.

## Prerequisites
* **Extensible Follower Framework** - Scripts must be extracted from the BSA.
  * **SkyUI 5** - Clone their git repository and copy dist/Data/Scripts from the working directory. SkyUI SDK does not have classes EFF needs.
  * **UIExtensions** - Again, scripts must be extracted from the BSA.
